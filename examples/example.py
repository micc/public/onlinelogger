from onlinelogger import OnlineLogger
import numpy as np
import matplotlib.pyplot as plt

hyper_params = {
    "sequence_length": 28,
    "input_size": 28,
    "hidden_size": 128,
    "num_layers": 2,
    "num_classes": 10,
    "batch_size": 100,
    "num_epochs": 3,
    "learning_rate": 0.01,
    "level": {
        "a": 1,
    }
}

import os

online_logger = OnlineLogger(
    params=hyper_params,
    wandb_project_name="Continual-Visual-Search"
)


for i in range(5):
    online_logger.log_metric("loss", i)

online_logger.log_metric("best", 5)
x = 20
# online_logger.log_text("text", f"{x}")
online_logger.log_text("text", 'long long text')
# img = np.zeros((5,5))
path_fig = 'img.png'
# plt.savefig(path_fig)
online_logger.log_image("adada", epoch=1, figure=path_fig)