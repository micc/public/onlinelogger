import os
import re
import codecs

from setuptools import find_packages, setup


def read(*parts):
    """
    Build an absolute path from *parts* and and return the contents of the
    resulting file.  Assume UTF-8 encoding.
    """
    with codecs.open(os.path.join(HERE, *parts), "rb", "utf-8") as f:
        return f.read()

###############################################################################


# Define Metadata
VERSION = "0.2"
TITLE = "Online Logger"
DESCRIPTION = "An Online logger for various frameworks"
URL = "https://www.micc.unifi.it"
URI = URL
DOC = DESCRIPTION + " <" + URI + ">"
AUTHOR = "Bruni, Biondi, Mugnai @ MICC"
AUTHOR_EMAIL = "name.surname@unifi.it"
MANTAINER = AUTHOR
MANTAINER_EMAIL = AUTHOR_EMAIL
LICENSE = "Closed Source"
COPYRIGHT = "Copyright (c) 2021 MICC"

###############################################################################

# name of the project, will be used in import
NAME = "onlinelogger"
PACKAGES = find_packages(where="src")

HERE = os.path.abspath(os.path.dirname(__file__))
KEYWORDS = ['online', 'logger', 'neptune', 'cometml']
PROJECT_URLS = {}
CLASSIFIERS = [
    "Natural Language :: English",
    'Development Status :: 3 - Alpha',
    "Intended Audience :: Developers",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 3",
]
with open('requirements.txt') as f:
    INSTALL_REQUIRES = f.read().splitlines()

EXTRAS_REQUIRE = {
    "tests": ["pytest"],
}

EXTRAS_REQUIRE["dev"] = (
    EXTRAS_REQUIRE["tests"] + ["pre-commit"]
)

###############################################################################


LONG = (
    read("README.md")
    + "\n\n"
    + "Release Information\n"
    + "===================\n\n"
    + "<{url}en/stable/changelog.html>`_.\n\n".format(url=URL)
    + read("AUTHORS.rst")
)


if __name__ == "__main__":
    setup(
        name=NAME,
        description=DESCRIPTION,
        license=LICENSE,
        url=URL,
        project_urls=PROJECT_URLS,
        version=VERSION,
        author=AUTHOR,
        author_email=AUTHOR_EMAIL,
        maintainer=MANTAINER,
        maintainer_email=MANTAINER_EMAIL,
        keywords=KEYWORDS,
        long_description=LONG,
        packages=PACKAGES,
        package_dir={"": "src"},
        python_requires=">=3.5",
        zip_safe=False,
        classifiers=CLASSIFIERS,
        install_requires=INSTALL_REQUIRES,
        extras_require=EXTRAS_REQUIRE,
        include_package_data=True,
        setup_requires=[
            'wheel',
            'setuptools>=40.1.0',
            'pytest-runner',
        ],
        tests_require=[
            'pytest',
        ],
)
