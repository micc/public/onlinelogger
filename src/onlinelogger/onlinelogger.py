import os
from codename import codename
import matplotlib.pyplot as plt
from typing import List
from pathlib import Path
from onlinelogger.wrappers.factory_wrapper import wrapper_factory

try:
    from comet_ml import Experiment as CometExperiment

    COMET_AVAILABLE = True
except ModuleNotFoundError:
    # Error handling
    COMET_AVAILABLE = False

try:
    import neptune.new as neptune

    NEPTUNE_AVAILABLE = True
except ModuleNotFoundError:
    NEPTUNE_AVAILABLE = False

try:
    import wandb

    WANDB_AVAILABLE = True
except ModuleNotFoundError:
    WANDB_AVAILABLE = False

import logging
logger = logging.getLogger(__name__)

class OnlineLogger:

    # API Neptune https://docs.neptune.ai/api-reference/index.html
    # API CometML https://www.comet.ml/docs/python-sdk/API/

    def __init__(
            self,
            params: dict,
            neptune_project_name: str = None,
            comet_project_name: str = None,
            comet_workspace: str = None,
            wandb_project_name: str = None,
            experiment_offline: bool = False,
            experiment_folder: str = None,
            experiment_resume: bool = False,
            experiment_codename: str = None,
            device_id: int = 0
    ):
        """            
            Set experiment folder to save exp id for future resume

        Args:
            params (dict): [description]
            neptune_project_name (str, optional): [description]. Defaults to None.
            comet_project_name (str, optional): [description]. Defaults to None.
            comet_workspace (str, optional): [description]. Defaults to None.
            experiment_offline (bool, optional): [description]. Defaults to False.
            experiment_folder (str, optional): [description]. Defaults to None.
            experiment_resume (bool, optional): [description]. Defaults to False.
            experiment_codename (str, optional): Friendly name for the exp. Auto generated if None. Defaults to None.
            device_id (int, optional): Device id to resume multiple online experiments during distributed training. 
                                       Defaults to 0 for sequentail training. 

        Raises:
            ValueError: [description]
        """

        # neptune
        self.neptune_experiment = None
        self.neptune_project_name = neptune_project_name

        # comet
        self.comet_experiment = None
        self.comet_project_name = comet_project_name
        self.comet_workspace = comet_workspace

        # wandb
        self.wandb_project_name = wandb_project_name

        # resume
        self.experiment_folder = experiment_folder
        self.experiment_resume = experiment_resume
        self.experiment_offline = experiment_offline

        # distributed
        self.device_id = device_id

        # generate codename
        if experiment_codename is not None:
            self.experiment_codename = experiment_codename.replace(' ', '-')
        else:
            self.experiment_codename = codename(separator='-', )

        if self.experiment_offline:
            if experiment_folder is None:
                raise ValueError("An Offline experiment need an experiment folder")
            else:
                self.offline_folder = os.path.join(self.experiment_folder, 'offline_dir')
                if not os.path.exists(self.offline_folder):
                    os.makedirs(self.offline_folder)
        else:
            self.offline_folder = None

        self.wrappers = {}

        if neptune_project_name and NEPTUNE_AVAILABLE:
            self.wrappers['neptune'] = wrapper_factory(
                name='neptune',
                params=params,
                project_name=neptune_project_name,
                experiment_offline=experiment_offline,
                experiment_folder=self.experiment_folder,
                experiment_resume=experiment_resume,
                experiment_codename=self.experiment_codename
            )

        if comet_project_name and comet_workspace and COMET_AVAILABLE:
            self.wrappers['comet'] = wrapper_factory(
                name='comet',
                params=params,
                project_name=comet_project_name,
                workspace=comet_workspace,
                experiment_offline=experiment_offline,
                experiment_folder=self.experiment_folder,
                experiment_resume=experiment_resume,
                experiment_codename=self.experiment_codename
            )
        if wandb_project_name and WANDB_AVAILABLE:
            self.wrappers['wandb'] = wrapper_factory(
                name='wandb',
                params=params,
                project_name=wandb_project_name,
                experiment_offline=experiment_offline,
                experiment_folder=self.experiment_folder,
                experiment_resume=experiment_resume,
                experiment_codename=self.experiment_codename
            )

    @property
    def wandb_id(self):
        return self.wrappers['wandb'].experiment_id

    @property
    def comet_id(self):
        return self.wrappers['comet'].experiment_id

    @property
    def neptune_id(self):
        return self.wrappers['neptune'].experiment_id

    @property
    def comet_url(self):
        return self.wrappers["comet"].experiment_url

    def log_metric(self, name, value, epoch=None):
        for wrapper_name, wrapper in self.wrappers.items():
            wrapper.log_metric(name=name, value=value, epoch=epoch)

    def log_image(self, name, epoch, figure, ext=None):
        for wrapper_name, wrapper in self.wrappers.items():
            wrapper.log_image(name=name, epoch=epoch, figure=figure, ext=ext)

    def log_embedding(self, features, labels):
        for wrapper_name, wrapper in self.wrappers.items():
            wrapper.log_embedding(features=features, labels=labels)

    def log_confusion_matrix(self, y_true, y_predicted, epoch):
        for wrapper_name, wrapper in self.wrappers.items():
            wrapper.log_confusion_matrix(y_true=y_true, y_predicted=y_predicted, epoch=epoch)

    def log_text(self, name, text):
        for wrapper_name, wrapper in self.wrappers.items():
            wrapper.log_text(name=name, text=text)

    def log_other(self, key, value):
        for wrapper_name, wrapper in self.wrappers.items():
            wrapper.log_other(key=key, value=value)

    def add_tag(self, tag: str):
        for wrapper_name, wrapper in self.wrappers.items():
            wrapper.add_tag(tag=tag)

    def add_tags(self, tags: List[str]):
        for wrapper_name, wrapper in self.wrappers.items():
            wrapper.add_tags(tags=tags)

    def cleanup_logs(self):
        for wrapper_name, wrapper in self.wrappers.items():
            wrapper.cleanup_logs()
