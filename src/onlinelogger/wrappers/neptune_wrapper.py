from .base_wrapper import BaseWrapper
import matplotlib.pyplot as plt
from typing import Union, List
from pathlib import Path
import glob
import os

try:
    import neptune.new as neptune
    from neptune.new.types import File as NeptuneFile
    from neptune import OfflineBackend as NeptuneOfflineExperiment  # TODO not still avaiable
    NEPTUNE_AVAILABLE = True
except ModuleNotFoundError:
    NEPTUNE_AVAILABLE = False

import logging
logger = logging.getLogger(__name__)


class NeptuneWrapper(BaseWrapper):

    def __init__(self, *args, **kwargs) -> None:

        if not NEPTUNE_AVAILABLE:
            raise ValueError("Neptune not installed!")
        
        super().__init__(*args, **kwargs)

        if self.experiment_offline:
            # FIXME nuovo questo sotto qua 
            # logger.warning("Neptune Offline Not Yet Available! Disabling Netpune...")
            # # C'È UN GROSSO PROBLEMA CON L'OFFLINE
            # # in pratica non fa scegliere la cartella dove mettere l'offline results, 
            # # ma di default usa delle costanti dove salva dei valori, attraverso
            # # run_path = "{}/{}/{}".format(NEPTUNE_RUNS_DIRECTORY, OFFLINE_DIRECTORY, api_run.uuid)
            # # e NEPTUNE_RUNS_DIRECTORY = '.neptune' OFFLINE_DIRECTORY = 'offline'.
            # # Non c'è modo di passarglielo come argomento si deve cambiare a mano
            # return
            # # neptune_project = nept.init(name='test', mode='offline')

            # FIXME VERSIONE VECCHIA DA QUA IN GIU
            if self.experiment_resume:
                # Offline Resume
                raise Exception("Netptune offline resume still not avaiable; please disable it.")
            else: 
                # Offline new Experiment
                from neptune import OfflineBackend
                neptune.init(self.project_name, backend=OfflineBackend())

        else:
            if self.experiment_resume:
                # Online Resume
                # https://docs.neptune.ai/logging-and-managing-experiment-results/updating-existing-experiment.html?highlight=resume
                neptune_file_id = glob.glob(os.path.join(self.experiment_folder, '*.neptune'))
                neptune_id, ext = neptune_file_id[0].split('/')[-1].split('.neptune')
                neptune_id, device_id = neptune_id.split('_')
                device_id = int(device_id.replace('id', ''))
                if device_id == self.device_id:
                    self.neptune_experiment = neptune.init(self.project_name, run=neptune_id) # NUOVO
                # else:
                    # raise ValueError(f"There should be one .nept file in {self.experiment_folder}!")
                    

            else:
                # # Online New Experiment 
                self.neptune_experiment = neptune.init(self.project_name, name=self.experiment_codename)
                self.neptune_experiment['parameters'] = self.params

        # save neptune id for later resume
        if self.experiment_folder:
            Path(os.path.join(self.experiment_folder, f'{self.experiment_id}_id{self.device_id}.neptune')).touch()


    @property
    def experiment_id(self):
        return self.neptune_experiment['sys/id'].fetch()

    def log_metric(self, name, value, epoch=None) -> None:
        self.neptune_experiment[name].log(value) 

    def log_image(self, name, epoch, figure, ext=None):
        self.neptune_experiment[name].upload(figure)

    def log_embedding(self, features, labels):
        return

    def log_confusion_matrix(self, y_true, y_predicted, epoch):
        return

    def log_text(self, name, text):
        self.neptune_experiment[name].log(text)   
        
    def log_other(self, key, value):
        return

    def add_tag(self, tag: str):
        self.neptune_experiment['sys/tags'].add(tag)  
    
    def add_tags(self, tags: List[str]):
        self.neptune_experiment['sys/tags'].add(tags) 
    
    def cleanup_logs(self):
        """ TODO """
        return 