import abc
import os
from typing import List

import logging
logger = logging.getLogger(__name__)

class BaseWrapper:

    def __init__(self,
        params: dict, 
        project_name: str = None,
        workspace: str = None,
        experiment_offline: bool = False,
        experiment_folder: str = None,
        experiment_resume: bool = False,
        experiment_codename: str = None,
        device_id: int = 0
    ) -> None:

        self.params = params
        self.project_name = project_name
        self.workspace = workspace
        self.experiment_offline = experiment_offline
        self.experiment_folder = experiment_folder
        self.experiment_resume = experiment_resume
        self.experiment_codename = experiment_codename
        self.device_id = device_id

        if experiment_folder:
            self.offline_folder = os.path.join(self.experiment_folder, 'offline_dir') 
        else:
            self.offline_folder = None

        
    @property
    @abc.abstractmethod
    def experiment_id(self):
        """Obtain Experiment ID from remote logger

        Raises:
            NotImplementedError: [description]
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def log_metric(self, name, value, epoch=None) -> None:
        """[summary]

        Args:
            name ([type]): [description]
            value ([type]): [description]
            epoch ([type], optional): [description]. Defaults to None.

        Raises:
            NotImplementedError: [description]
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def log_image(self, name, epoch, figure, ext=None):
        """[summary]

        Args:
            name ([type]): [description]
            epoch ([type]): [description]
            figure ([type]): [description]
            ext ([type], optional): [description]. Defaults to None.

        Raises:
            NotImplementedError: [description]
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def log_embedding(self, features, labels):
        """[summary]

        Args:
            features ([type]): [description]
            labels ([type]): [description]

        Raises:
            NotImplementedError: [description]
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def log_confusion_matrix(self, y_true, y_predicted, epoch):
        """[summary]

        Args:
            y_true ([type]): [description]
            y_predicted ([type]): [description]
            epoch ([type]): [description]

        Raises:
            NotImplementedError: [description]
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def log_text(self, name, text):
        """[summary]

        Args:
            name ([type]): [description]
            text ([type]): [description]

        Raises:
            NotImplementedError: [description]
        """
        raise NotImplementedError()
        
    @abc.abstractmethod
    def log_other(self, key, value):
        """[summary]

        Args:
            key ([type]): [description]
            value ([type]): [description]

        Raises:
            NotImplementedError: [description]
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def add_tag(self, tag: str):
        """[summary]

        Args:
            tag (str): [description]

        Raises:
            NotImplementedError: [description]
        """
        raise NotImplementedError()
    
    @abc.abstractmethod
    def add_tags(self, tags: List[str]):
        """[summary]

        Args:
            tags (List[str]): [description]

        Raises:
            NotImplementedError: [description]
        """
        raise NotImplementedError()
    
    @abc.abstractmethod
    def cleanup_logs(self):
        """[summary]
        Args:
            tags (List[str]): [description]

        Raises:
            NotImplementedError: [description]
        """
        raise NotImplementedError()