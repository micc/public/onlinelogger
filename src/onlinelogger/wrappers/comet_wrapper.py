from .base_wrapper import BaseWrapper
import matplotlib.pyplot as plt
from typing import Union, List
from pathlib import Path
import glob
import os

try:
    from comet_ml import Experiment as CometExperiment
    from comet_ml import ExistingExperiment as CometExistingExperiment
    from comet_ml import OfflineExperiment as CometOfflineExperiment
    from comet_ml.offline import ExistingOfflineExperiment as CometExistingOfflineExperiment
    COMET_AVAILABLE = True
except ModuleNotFoundError:
    # Error handling
    COMET_AVAILABLE = False

import logging
logger = logging.getLogger(__name__)


class CometWrapper(BaseWrapper):

    def __init__(self, *args, **kwargs) -> None:

        if not COMET_AVAILABLE:
            raise ValueError("Comet not installed!")
        
        super().__init__(*args, **kwargs)
        
        if self.experiment_offline:

            if self.experiment_resume:
                # Offline Resume
                comet_file_id = glob.glob(os.path.join(self.experiment_folder, '*.comet'))
                if len(comet_file_id) == 1:
                    comet_id, ext = comet_file_id[0].split('.comet')
                    self.comet_experiment = CometExistingOfflineExperiment(
                        previous_experiment=comet_id,
                        offline_directory=self.offline_folder,
                        auto_output_logging="simple"
                    )
                else:
                    raise ValueError(f"There should be one .comet file in {self.experiment_folder}!")

            else:
                # Offline New Experiment
                self.comet_experiment = CometOfflineExperiment(
                    project_name=self.project_name,
                    workspace=self.workspace,
                    offline_directory=self.offline_folder,
                    auto_output_logging="simple"
                )

        else:

            if self.experiment_resume:
                # Online Resume
                comet_file_id = glob.glob(os.path.join(self.experiment_folder, '*.comet'))
                comet_id, ext = comet_file_id[0].split('/')[-1].split('.comet')
                comet_id, device_id = comet_id.split('_')
                device_id = int(device_id.replace('id', ''))
                if device_id == self.device_id:
                    self.comet_experiment = CometExistingExperiment(
                        previous_experiment=comet_id,
                        auto_output_logging="simple"
                    )
                # else:
                #     raise ValueError(f"There should be one .comet file in {self.experiment_folder}!")

            else:
                # Online New Experiment
                self.comet_experiment = CometExperiment(
                    project_name=self.project_name,
                    workspace=self.workspace,
                    auto_output_logging="simple")
                if kwargs.get('experiment_codename', None) is not None:
                    self.comet_experiment.set_name(kwargs['experiment_codename'])

        # save id for future resume
        if self.experiment_folder:
            Path(os.path.join(self.experiment_folder, f'{self.experiment_id}_id{self.device_id}.comet')).touch()

        from onlinelogger.utils import flatten
        self.comet_experiment.log_parameters(flatten(self.params))


    @property
    def experiment_id(self):
        return self.comet_experiment.id

    @property
    def experiment_url(self):
        return self.comet_experiment.url

    def log_metric(self, name, value, epoch=None) -> None:
        self.comet_experiment.log_metric(name=name,
                                         value=value,
                                         epoch=epoch,
                                         step=epoch,
                                         include_context=True)

    def log_image(self, name, epoch, figure, ext=None):
        if isinstance(figure, str):
            figure = plt.imread(figure)

        self.comet_experiment.log_image(image_data=figure, step=epoch, name=name)

    def log_embedding(self, features, labels):
        self.comet_experiment.log_embedding(features, labels)

    def log_confusion_matrix(self, y_true, y_predicted, epoch):
        self.comet_experiment.log_confusion_matrix(
            title=f"Confusion Matrix, Epoch {epoch}",
            y_true=y_true,
            y_predicted=y_predicted,
            file_name=f"confusion-matrix-{epoch}.json",
            step=epoch
        )

    def log_text(self, name, text):
        self.comet_experiment.log_text(text)  
        
    def log_other(self, key, value):
        self.comet_experiment.log_other(key, value)

    def add_tag(self, tag: str):
        self.comet_experiment.add_tag(tag) 
    
    def add_tags(self, tags: List[str]):
        self.comet_experiment.add_tags(tags)
    
    def cleanup_logs(self):
        self.comet_experiment.end()
