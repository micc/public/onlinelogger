from .base_wrapper import BaseWrapper
import matplotlib.pyplot as plt
from typing import Union, List
from pathlib import Path
import glob
import os

try:
    import wandb

    WANDB_AVAILABLE = True
except ModuleNotFoundError:
    WANDB_AVAILABLE = False

import logging
logger = logging.getLogger(__name__)


class WandbWrapper(BaseWrapper):

    def __init__(self, *args, **kwargs) -> None:

        if not WANDB_AVAILABLE:
            raise ValueError("wandb not installed!")

        super().__init__(*args, **kwargs)

        wandb.login()

        if self.experiment_offline:
            if self.experiment_resume:
                # Offline Resume
                raise Exception("wandb offline resume still not avaiable; please disable it.")
            else:
                # Offline new Experiment
                self.wandb_experiment = wandb.init(project=self.project_name,
                                                   config=self.params,
                                                   name=self.experiment_codename,
                                                   mode="offline",
                                                   resume='allow')

        else:
            if self.experiment_resume:
                # Online Resume
                wandb_file_id = glob.glob(os.path.join(self.experiment_folder, '*.wandb'))
                wandb_id, ext = wandb_file_id[0].split('/')[-1].split('.wandb')
                wandb_id, device_id = wandb_id.split('_')
                device_id = int(device_id.replace('id', ''))
                if device_id == self.device_id:
                    self.wandb_experiment = wandb.init(project=self.project_name,
                                                       config=self.params,
                                                       name=self.experiment_codename,
                                                       resume='must',
                                                       id=wandb_id
                                                       )
                # else:
                #     raise ValueError(f"There should be one .wandb file in {self.experiment_folder}!")

            else:
                logger.info(f"Using wandb. Logging value to {self.project_name}")
                self.wandb_experiment = wandb.init(project=self.project_name,
                                                   config=self.params,
                                                   name=self.experiment_codename,
                                                   resume='allow')

            # save id for future resume
            if self.experiment_folder:
                Path(os.path.join(self.experiment_folder, f'{self.experiment_id}_id{self.device_id}.wandb')).touch()

    @property
    def experiment_id(self):
        return self.wandb_experiment.id

    def log_metric(self, name, value, epoch=None) -> None:
        self.wandb_experiment.log({f"{name}": value})

    def log_image(self, name, epoch, figure, ext=None):
        self.wandb_experiment.log({f"{name}": wandb.Image(figure)})

    def log_embedding(self, features, labels):
        return

    def log_confusion_matrix(self, y_true, y_predicted, epoch):
        return

    def log_text(self, name, text):
        #self.wandb_experiment.log({f"{name}": text})  # TODO DOESNT WORK!!!!!
        # a solution can be logterm()
        pass

    def log_other(self, key, value):
        return

    def add_tag(self, tag: str):
        return

    def add_tags(self, tags: List[str]):
        return

    def cleanup_logs(self):
        """ TODO """
        return 