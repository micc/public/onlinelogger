import importlib

__factory = {
    'neptune': 'onlinelogger.wrappers.neptune_wrapper.NeptuneWrapper',
    'comet': 'onlinelogger.wrappers.comet_wrapper.CometWrapper',
    'wandb': 'onlinelogger.wrappers.wandb_wrapper.WandbWrapper'
}

available_wrappers = __factory.keys()


def wrapper_factory(name, **kwargs):
    if name not in __factory.keys():
        raise KeyError("Unknown wrapper: {}".format(name))

    module_path = '.'.join(__factory[name].split('.')[:-1])
    module = importlib.import_module(module_path)
    class_name = __factory[name].split('.')[-1]
    return getattr(module, class_name)(**kwargs)
