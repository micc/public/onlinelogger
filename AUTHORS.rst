
Credits
=======

Initially written and maintained by `Matteo Bruni <matteo.bruni@unifi.it>`_.

Contributors:
- `Niccolò Biondi <niccolo.biondi@unifi.it>`
- `Daniele Mugnai <daniele.mugnai@unifi.it>`




